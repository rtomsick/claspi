package net.tomsick.claspi.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import net.tomsick.claspi.Discoverable;
import net.tomsick.claspi.Loader;
import net.tomsick.claspi.test.service.TestService;
import net.tomsick.claspi.test.service.TestServiceForServiceLoader;
import net.tomsick.claspi.test.service.TestServiceForServiceLoaderImpl;

public class DefaultLoaderTest
{
    @Test
    public void 
    loadAnnotatedWithNoArgs()
    {
        final Loader loader = 
            Loader.builder()
                    .withAnnotations(Discoverable.class)
                    .build();

        Iterable<TestService> found = 
            loader.load(TestService.class, this.getClass().getClassLoader());

        List<TestService> foundList = new ArrayList<>();
        found.forEach(foundList :: add);

        Assert.assertEquals("Found <> 2 impls", 2, foundList.size());

        found.forEach(s -> Assert.assertTrue(s instanceof TestService));
    }

    @Test
    public void
    loadAnnotatedWithConstructorArgs()
    {
        final Loader loader = 
            Loader.builder()
                    .withAnnotations(Discoverable.class)
                    .build();

        Iterable<TestService> found = 
            loader.load(TestService.class, this.getClass().getClassLoader(),
                        (x) -> new String[]{"test"});

        List<TestService> foundList = new ArrayList<>();
        found.forEach(foundList :: add);

        Assert.assertEquals("Found <> 1 impl", 1, foundList.size());

        found.forEach(s -> Assert.assertTrue(s instanceof TestService));
    }

    @Test
    public void
    loadWithNoAnnotations()
    {
        final Loader loader = Loader.builder().build();

        final List<TestService> foundList = new ArrayList<>();

        loader.load(TestService.class, this.getClass().getClassLoader())
                    .forEach(foundList :: add);


        Assert.assertEquals("Found <> 3 impls", 3, foundList.size());

        foundList.forEach(s -> Assert.assertTrue(s instanceof TestService));
    }

    @Test
    public void
    loadViaJavaSPIDeclaration()
    {
        final Loader loader = 
            Loader.builder()
                    .scanClasses(false)
                    .useServicesDeclaration(true)
                    .build();

        final List<TestServiceForServiceLoader> foundList = new ArrayList<>();

        loader.load(TestServiceForServiceLoader.class, 
                        this.getClass().getClassLoader())
                .forEach(foundList :: add);

        Assert.assertEquals(1, foundList.size());

        Assert.assertTrue("Failed to find impl declared in " + 
                            "'META-INF/services/TestService' ", 
            foundList.get(0) instanceof TestServiceForServiceLoaderImpl);
    }
}