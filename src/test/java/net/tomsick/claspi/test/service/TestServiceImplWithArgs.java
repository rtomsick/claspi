package net.tomsick.claspi.test.service;

import net.tomsick.claspi.Discoverable;

@Discoverable
public final class TestServiceImplWithArgs
implements TestService
{
    public TestServiceImplWithArgs()
    {
        this("test");
    }

    public TestServiceImplWithArgs(String test)
    {
        
    }

	@Override
	public void doNothing() {
		
	}
    
}