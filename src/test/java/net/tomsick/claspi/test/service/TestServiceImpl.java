package net.tomsick.claspi.test.service;

import net.tomsick.claspi.Discoverable;

@Discoverable
public final class TestServiceImpl
implements TestService
{

	@Override
	public void 
	doNothing()
	{
		
	}

}