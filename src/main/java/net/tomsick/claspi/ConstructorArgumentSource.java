package net.tomsick.claspi;

@FunctionalInterface
public interface ConstructorArgumentSource
{
    public Object[] arguments(Class<?> cls);
}