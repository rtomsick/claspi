package net.tomsick.claspi;

/**
 * Convenience annotation; used to indicate that an annotated class should be
 * "discoverable" by code using CLaSPI.  CLaSPI does not require use of this
 * annotation, it is provided merely as a convenient canonical way to indicate
 * that a component should be eligible for discovery by code using CLaSPI.
 * 
 * @author Rob (robert@tomsick.net)
 */
public @interface Discoverable
{
    
}