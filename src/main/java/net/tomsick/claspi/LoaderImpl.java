package net.tomsick.claspi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import io.github.lukehutch.fastclasspathscanner.scanner.ScanResult;

class LoaderImpl 
implements Loader
{
    private static final String		        SERVICES_PATH = 
        "META-INF/services/";

    private final List<String>              excludedPackages;
    private final List<URL>                 additionalPaths;
    private final Class<?>[]                requiredAnnotations;
    private final boolean                   useServicesDeclaration;
    private final boolean                   scanClasses;
    private final Optional<ClassLoader>     scanLoader;

    LoaderImpl(List<String> exclusions, List<URL> additionalPaths, 
                    List<Class<?>> requiredAnnotations,
                    boolean useServicesDeclaration,
                    boolean scanClasses, ClassLoader scanUsing) 
    {
        this.additionalPaths = 
            Collections.unmodifiableList(new ArrayList<>(additionalPaths));
        this.excludedPackages = 
            Collections.unmodifiableList(new ArrayList<>(exclusions));
        this.requiredAnnotations = 
            requiredAnnotations.toArray(new Class<?>[0]);

        /* validate that annotations are actually annotations */
        for (Class<?> a : this.requiredAnnotations)
        {
            if (! a.isAnnotation())
            {
                throw new IllegalArgumentException(
                    String.format("Required annotation '%s' was not " + 
                                    "annotation", a.getCanonicalName()));
            }
        }

        this.useServicesDeclaration = useServicesDeclaration;
        this.scanClasses = scanClasses;
        this.scanLoader = Optional.ofNullable(scanUsing);
    }
    
    @Override
    public <T> Iterable<T> 
    load(Class<T> iface, ClassLoader classLoader, 
            ConstructorArgumentSource argumentSource)
    {
        if (iface == null || ! iface.isInterface())
        {
            throw new IllegalArgumentException(
                        "Interface class must be interface");
        }

        final ClassLoader loader = this.createLoader(classLoader);
        Set<String> implNames = new HashSet<>();
        if (this.scanClasses)
        {
            ClassLoader scanLoader = this.scanLoader.orElse(loader);
            final FastClasspathScanner scanner = this.createScanner(scanLoader);

            /* TODO - optimize by only scanning classes that have our annotations */

            final ScanResult result = scanner.scan();

            Set<String> annotated = new HashSet<>();

            if (this.requiredAnnotations.length > 0)
            {
                annotated = 
                    new HashSet<>(result.getNamesOfClassesWithAnnotationsAllOf(
                                            this.requiredAnnotations));
            }

            final Predicate<String> aFilter = 
                annotated.isEmpty()
                    ? (x) -> true 
                    : annotated :: contains;

            implNames = 
                result.getNamesOfClassesImplementing(iface)
                        .stream()
                        .filter(aFilter)
                        .collect(Collectors.toSet());
        }

        /* load from META-INF/services */
        if (this.useServicesDeclaration)
        {
            try 
            {
                final Enumeration<URL> urls = 
                        loader.getResources(SERVICES_PATH + iface.getName());
                            
                while (urls.hasMoreElements())
                {
                    try (final Reader reader = 
                            new InputStreamReader(urls.nextElement()
                                                        .openStream(),
                                                Charset.defaultCharset());
                        final BufferedReader br = new BufferedReader(reader))
                    {
                        br.lines().forEach(implNames :: add);
                    }
                }
            }
            catch (IOException e)
            {
                throw new CLInstantiationException(
                            "Failed to load service definition", e);
            }
        }

        if (implNames.isEmpty())
        {
            return Collections.emptyList();
        }

        List<Class<?>> loaded = new ArrayList<>();

        for (String cn : implNames)
        {
            final String className = cn.trim();
            try
            {
                loaded.add(loader.loadClass(className));
            }
            catch (ClassNotFoundException e)
            {
                throw new CLInstantiationException(
                    String.format("Unable to load class '%s'", className), e);
            }
        }

        List<T> instantiated = new ArrayList<>();

        for (Class<?> cls : loaded)
        {
            try
            {
                T obj = null;
                try
                {
                    /* arguments to constructor */
                    if (argumentSource != null)
                    {
                        Object[] args = argumentSource.arguments(cls);

                        Constructor<?> ctor = 
                            matchConstructor(cls.getConstructors(), args);

                        if (ctor == null)
                        {
                            continue;
                        }

                        if (ctor.getParameterCount() < 1)
                        {
                            obj = iface.cast(cls.newInstance());
                        }
                        else
                        {
                            obj = iface.cast(ctor.newInstance(args));
                        }
                    }
                    else
                    {
                        /* default constructor */
                        obj = iface.cast(cls.newInstance());
                    }
                }
                catch (ClassCastException cce)
                {
                    obj = null;
                }

                if (obj != null)
                {
                    instantiated.add(obj);
                }

            } 
            catch (InstantiationException | InvocationTargetException 
                    | IllegalAccessException e)
            {
				throw new CLInstantiationException(
                            "Failed to instantiate new instance", e);
			}
        }

        return instantiated;
    }

    private final ClassLoader
    createLoader(ClassLoader loader)
    {
        if (loader == null)
        {
            loader = Loader.class.getClassLoader();
        }
        return new URLClassLoader(
                        this.additionalPaths.toArray(new URL[0]), loader);
    }
    
    /* safe, provided our callers parameterize correctly */
    @SuppressWarnings("unchecked")
    private static final <T> Constructor<T> 
    matchConstructor(Constructor<?>[] constructors, Object[] arguments)
    {
        if (arguments == null || arguments.length < 1)
        {
            for (Constructor<?> c : constructors)
            {
                if (c.getParameters() == null || c.getParameters().length < 1)
                {
                    return (Constructor<T>) c;
                }
            }
            
            return null;
        }

        Class<?>[] argumentTypes = 
            Stream.of(arguments)
                    .map(a -> a.getClass())
                    .collect(Collectors.toList()).toArray(new Class<?>[0]);

        List<Constructor<?>> ctors = 
            Stream.of(constructors)
                    .filter(ctor -> Arrays.equals(ctor.getParameterTypes(), 
                                                    argumentTypes))
                    .collect(Collectors.toList());
        
        final int found = ctors.size();
        
        if (found < 1)
        {
            return null;
        }
        else if (found > 1)
        {
            throw new CLInstantiationException(
                        "Found > 1 constructor matching argument types.  "
                        + "This is impossible.");
        }

        return (Constructor<T>) ctors.get(0);
	}

    
    private FastClasspathScanner 
    createScanner(ClassLoader loader)
    {
        final String exclusions = 
            this.excludedPackages
                .stream()
                .map(s -> "-" + s)
                .collect(Collectors.joining(","));

        FastClasspathScanner scanner = new FastClasspathScanner(exclusions);

        return scanner.overrideClassLoaders(loader);
    }

    public static final class CLInstantiationException
    extends RuntimeException
    {
        private static final long serialVersionUID = 1L;

        CLInstantiationException(String msg)
        {
            super(msg);
        }

		CLInstantiationException(String msg, Throwable cause)
        {
            super(msg, cause);
        }
    }

}
