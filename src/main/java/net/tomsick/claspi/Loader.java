package net.tomsick.claspi;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Service loader interface.
 * 
 * @author Rob (robert@tomsick.net)
 */
public interface Loader
{
    /**
     * Load and instantiate discovered implementations of the given interface.  
     * Classloading will be done on-demand by the given classloader.
     * 
     * @param iface interface of service to load, not {@code null}
     * @param classLoader loader to use for loading classes, or {@code null}
     * if the classloader used to load {@link Loader} should be used
     * 
     * @return instances of implementations of the specified interface, 
     * not {@code null}
     * 
     * @param <T> type of service
     */
    @NotNull
    public default <T> Iterable<T> 
    load(@NotNull Class<T> iface, ClassLoader classLoader)
    {
        ClassLoader cl = classLoader;

        if (cl == null)
        {
            cl = this.getClass().getClassLoader();
        }

        return this.load(iface, cl, (x) -> new Object[0]);
    }

    /**
     * Load and instantiate discovered implementations of the given interface.  
     * Classloading will be done on-demand by the given classloader.  
     * Parameters for implementation constructors will be provided by the 
     * given argument source.
     * 
     * @param iface interface of service to load, not {@code null}
     * @param classLoader loader to use for loading classes, or {@code null}
     * if the classloader used to load {@link Loader} should be used
     * @param argSource argument source, not {@code null}
     * 
     * @return instances of implementations of the specified interface, 
     * not {@code null}
     * 
     * @param <T> service type
     */
    @NotNull
    public <T> Iterable<T> 
    load(@NotNull Class<T> iface, ClassLoader classLoader, 
            @NotNull ConstructorArgumentSource argSource);

    /**
     * Get a builder to configure and construct instances of a {@link Loader}.
     * 
     * @return builder, not {@code null}
     */
    public static LoaderBuilder
    builder()
    {
        return new LoaderBuilder();
    }

    /**
     * Builder for {@link Loader}s.
     * 
     */
    public static final class LoaderBuilder
    {
        private List<String>      excludedPackages = new ArrayList<>();
        private List<URL>         additionalPaths = new ArrayList<>();
        private List<Class<?>>    requiredAnnotations = new ArrayList<>();
        private boolean           scanClasses = true;
        private boolean           useServicesDeclaration = false;
        private ClassLoader       scanLoader;

        /**
         * Specify the annotations which must be present on implementations
         * in order for them to be eligible for detection and construction.
         * This is an additive operation; annotations provided to this 
         * method will be required along with any already specified by the 
         * builder.
         * 
         * @param annotations annotations, not {@code null}
         * @return builder
         */
        public LoaderBuilder
        withAnnotations(@NotNull Collection<Class<?>> annotations)
        {
            this.requiredAnnotations.addAll(annotations);
            return this;
        }

        /**
         * Specify the annotations which must be present on implementations
         * in order for them to be eligible for detection and construction.
         * This is an additive operation; annotations provided to this 
         * method will be required along with any already specified by the 
         * builder.
         * 
         * @param annotations annotations, entries must not be null
         * @return builder
         */
        public LoaderBuilder
        withAnnotations(Class<?> ... annotations)
        {
            return this.withAnnotations(Arrays.asList(annotations));
        }

        /**
         * <p>
         * Paths which should be included during the search for 
         * implementations.  These will be considered in addition to those 
         * considered by the default classloader.
         * </p>
         * 
         * <p>
         * This is an additive operation; paths provided to this 
         * method will be required along with any already specified by the 
         * builder.
         * </p>
         * 
         * <p>
         * If specified, overrides {@link #scanUsing(ClassLoader)} (if already 
         * set.)
         * </p>
         * 
         * @param paths paths, not {@code null}
         * @return builder
         */
        public LoaderBuilder
        withAdditionalPaths(URL ... paths)
        {
            return this.withAdditionalPaths(Arrays.asList(paths));
        }

        /**
         * <p>
         * Paths which should be included during the search for 
         * implementations.  These will be considered in addition to those 
         * considered by the default classloader.
         * </p>
         * 
         * <p>
         * This is an additive operation; paths provided to this 
         * method will be required along with any already specified by the 
         * builder.
         * </p>
         * 
         * <p>
         * If specified, overrides {@link #scanUsing(ClassLoader)} (if already 
         * set.)
         * </p>
         * 
         * @param paths paths, not {@code null}
         * @return builder
         */
        public LoaderBuilder
        withAdditionalPaths(@NotNull Collection<URL> paths)
        {
            this.additionalPaths.addAll(paths);
            this.scanLoader = null;
            return this;
        }

        /**
         * <p>
         * Packages which should be excluded during the search for 
         * implementations.
         * </p>
         * 
         * <p>
         * This is an additive operation; packages provided to this 
         * method will be required along with any already specified by the 
         * builder.
         * </p>
         * 
         * @param packageNames package names
         * @return builder
         */
        public LoaderBuilder
        excludePackages(String ... packageNames)
        {
            return this.excludePackages(Arrays.asList(packageNames));
        }

        /**
         * <p>
         * Packages which should be excluded during the search for 
         * implementations.
         * </p>
         * 
         * <p>
         * This is an additive operation; packages provided to this 
         * method will be required along with any already specified by the 
         * builder.
         * </p>
         * 
         * @param packageNames package names
         * @return builder
         */
        public LoaderBuilder
        excludePackages(Collection<String> packageNames)
        {
            this.excludedPackages.addAll(packageNames);
            return this;
        }

        /**
         * Whether to support {@link java.util.ServiceLoader}-style services
         * declaration (via 'META-INF/services/').
         * 
         * @param use <code>true</code> to support this mechanism, 
         * <code>false</code> otherwise
         * 
         * @return builder
         */
        public LoaderBuilder
        useServicesDeclaration(boolean use)
        {
            this.useServicesDeclaration = use;
            return this;
        }

        /**
         * Whether to perform any class scanning.
         * 
         * @param scan <code>true</code> to perform class scanning,
         * <code>false</code> otherwise
         * 
         * @return builder
         */
        public LoaderBuilder
        scanClasses(boolean scan)
        {
            this.scanClasses = scan;
            return this;
        }

        /**
         * The classloader to use when scanning classes.  If used, will 
         * override any {@link #withAdditionalPaths(URL...) additional paths}, 
         * as the default classpath will no longer be considered.
         * 
         * @param loader class loader to use, not {@code null}
         * @return builder
         */
        public LoaderBuilder
        scanUsing(ClassLoader loader)
        {
            this.additionalPaths.clear();
            this.scanLoader = loader;
            return this;
        }

        /**
         * Build a loader with the properties set on this builder.
         * 
         * @return loader, not {@code null}
         */
        public Loader
        build()
        {
            return new LoaderImpl(this.excludedPackages, 
                                    this.additionalPaths, 
                                    this.requiredAnnotations,
                                    this.useServicesDeclaration,
                                    this.scanClasses, this.scanLoader);
        }

    }
}