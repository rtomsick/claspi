# CLaSPI

`CLaSPI` - ClassLoader-agnostic SPI

`CLaSPI` is intended for cases where OSGi is too heavy-weight and complex, and
where Spring is not an option (or does not provide the features that you
need).

It is backwards-compatible with Java's `ServiceLoader` approach (declaration
in `META-INF`), as well as supporting a modern, annotation-driven style.

## Usage

Some good examples of usage may be found beneath the `src/test` directory.  
It's not a lot of code, so it should be pretty easy to grok.

### Use Case

The ideal use case for `CLaSPI` is this:

You're building an application.  You need to allow for third-party or
out-of-tree implementations of some interface, loaded from *n* non-classpath
locations, but you want to use a different `ClassLoader` than the default one
to load said implementations.  (This is a fairly common situation in the web
app world.)

If you need hot loading/unloading, JMX support, etc. then this is not the
solution for you.  It is intended to be simple and limited in scope.  OSGi et
al. do a good job of solving those related problems, so use that instead!

Basically, `CLaSPI` is designed for use anywhere that you might use Java's SPI
mechanism (`ServiceLoader`); anything past that is best saved for another
solution.

## Bugs

Possibly, but again: it's not a lot of code, and the approach itself is pretty
battle-tested.  Frankly, the only reason that this library even exists is that
I noticed I was writing a version of this for project after project...